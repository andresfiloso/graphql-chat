const http = require("http");
const { ApolloServer } = require("apollo-server-express");
const cors = require("cors");
const express = require("express");
const expressJwt = require("express-jwt");
const jwt = require("jsonwebtoken");
const app = express();
const User = require("./models/User");
const dataSources = require("./dataSources");
const mongo = require("./services/mongo");

app.use(cors());
app.use(express.json());

const typeDefs = require("./typeDefs");
const resolvers = require("./resolvers");

app.post("/login", async (req, res) => {
  const { name, password } = req.body;
  const user = await User.findOne({ name });
  if (!(user && user.password === password)) {
    res.sendStatus(401);
    return;
  }
  const token = jwt.sign(
    { id: user.id, name: user.name },
    process.env.JWT_SECRET
  );
  res.send({ token });
});

const context = ({ req, connection }) => {
  // subscriptions
  if (connection && connection.context && connection.context.accessToken) {
    const { accessToken } = connection.context;
    const decodedToken = jwt.verify(accessToken, process.env.JWT_SECRET);
    return { userId: decodedToken.id, name: decodedToken.name };
  }

  // common requests
  if (req && req.headers && req.headers.authorization) {
    const accessToken = req.headers.authorization.split("Bearer ")[1];
    const decodedToken = jwt.verify(accessToken, process.env.JWT_SECRET);
    return { userId: decodedToken.id, name: decodedToken.name };
  }
  return {};
};

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context,
  dataSources,
});
apolloServer.applyMiddleware({ app, path: "/graphql" });

const httpServer = http.createServer(app);
apolloServer.installSubscriptionHandlers(httpServer);

app.use(
  expressJwt({
    secret: process.env.JWT_SECRET,
    algorithms: ["HS256"],
  })
);

module.exports = httpServer;
