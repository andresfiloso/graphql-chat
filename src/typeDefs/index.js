const { gql } = require("apollo-server-express");

module.exports = gql`
  type Query {
    messages: [Message]
  }

  type Mutation {
    addMessage(input: MessageInput!): Message
  }

  type Subscription {
    messageAdded: Message
  }

  type Message {
    id: ID!
    from: String
    text: String
  }

  input MessageInput {
    text: String
  }
`;
