const mongoose = require("mongoose");

console.log(process.env.MONGO_URI);
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverSelectionTimeoutMS: 3000,
  })
  .then(() => console.log(`MongoDB connected on ${process.env.MONGO_URI}`))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });

module.exports = mongoose.connection;
