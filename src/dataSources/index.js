const User = require("../models/User");
const Message = require("../models/Message");

const Users = require("./Users");
const Messages = require("./Messages");

module.exports = () => ({
  users: new Users(User),
  messages: new Messages(Message),
});
