const { MongoDataSource } = require("apollo-datasource-mongodb");

class Messages extends MongoDataSource {
  async getMessage(id) {
    return await this.model.findById(id);
  }

  async getMessages() {
    return this.model.find().lean();
  }

  async addMessage({ from, text }) {
    const message = new this.model({ from, text });
    const messageId = await message.save();
    return messageId;
  }
}

module.exports = Messages;
