const { MongoDataSource } = require("apollo-datasource-mongodb");

class Users extends MongoDataSource {
    getUser(name) {
      return this.findOne(name);
    }
  };
  

module.exports = Users;