const requireAuth = (userId) => {
    if (!userId) {
      throw new Error("Unauthorized");
    }
  };


  module.exports = {
    requireAuth
};