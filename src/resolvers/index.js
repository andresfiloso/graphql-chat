const { PubSub } = require("graphql-subscriptions");
const { MESSAGE_ADDED } = require("../utils/constants");
const { requireAuth } = require("../utils/auth");

const pubSub = new PubSub();

const Query = {
  messages: async (_root, _args, { userId, dataSources: { messages } }) => {
    requireAuth(userId);
    const msgs = await messages.getMessages();
    console.log(msgs);
    return msgs;
  },
};

const Mutation = {
  addMessage: async (_root, { input }, { userId, name, dataSources: { messages } }) => {
    requireAuth(userId);
    const messageId = await messages.addMessage({ from: name, text: input.text });
    const message = messages.getMessage(messageId);
    pubSub.publish(MESSAGE_ADDED, { messageAdded: message });
    return message;
  },
};

const Subscription = {
  messageAdded: {
    subscribe: (_root, _args, { userId }) => {
      requireAuth(userId);
      return pubSub.asyncIterator(MESSAGE_ADDED);
    },
  },
};

module.exports = { Query, Mutation, Subscription };
